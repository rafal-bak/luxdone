using Microsoft.VisualStudio.TestTools.UnitTesting;
using NBP.Common;
using NBP.Models;
using System;
using System.Collections.Generic;

namespace NBPTest
{
    [TestClass]
    public class HelperTest
    {
        private List<Rate> _rates;

        public HelperTest()
        {
            InitializeRates();
        }

        private void InitializeRates()
        {
            _rates = new List<Rate>
            {
                new Rate
                {
                    no = "019/C/NBP/2013",
                    effectiveDate = new DateTime(2013,1,28),
                    bid = 4.1301,
                    ask = 4.2135
                },
                new Rate
                {
                    no = "020/C/NBP/2013",
                    effectiveDate = new DateTime(2013,1,29),
                    bid = 4.1621,
                    ask = 4.2461
                },
                new Rate
                {
                    no = "021/C/NBP/2013",
                    effectiveDate = new DateTime(2013,1,30),
                    bid = 4.1530,
                    ask = 4.2370
                },
                new Rate
                {
                    no = "022/C/NBP/2013",
                    effectiveDate = new DateTime(2013,1,31),
                    bid = 4.1569,
                    ask = 4.2409
                }
            };
        }

        [TestMethod]
        public void CalculateAveragePriceTest()
        {
            var actualAverage = Helper.CalculateAveragePrice(_rates);
            var expectedAverage = 4.150525;
            Assert.AreEqual(expectedAverage, actualAverage);
        }       
        
        [TestMethod]
        public void CalculateStandardDeviationTest()
        {
            var actualStandardDeviation = Helper.CalculateStandardDeviation(_rates);
            var expectedStandardDeviation = 0.012477053939129816;
            Assert.AreEqual(expectedStandardDeviation, actualStandardDeviation);
        }
    }
}
