﻿using NBP.Models;
using System;
using System.Collections.Generic;

namespace NBP.Common
{
    public static class Helper
    {
        public static double CalculateStandardDeviation(List<Rate> rates)
        {
            double sum = 0;

            foreach (var rate in rates)
            {
                sum += rate.ask;
            }

            double arithmeticAverage = sum / rates.Count;
            sum = 0;

            foreach (var rate in rates)
            {
                sum += Math.Pow(rate.ask, 2);
            }

            double arithmeticAverage2 = sum / rates.Count;

            double standardDeviation = Math.Sqrt(arithmeticAverage2 - Math.Pow(arithmeticAverage, 2));

            return standardDeviation;
        }

        public static double CalculateAveragePrice(List<Rate> rates)
        {
            double sum = 0;
            foreach (var rate in rates)
            {
                sum += rate.bid;
            }

            return sum / rates.Count;
        }
    }
}
