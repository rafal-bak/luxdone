﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;

namespace NBP.Validations
{
    public class InputDataValidation : ActionFilterAttribute
    {
        private List<string> supportedCurrencies = new List<string> { "USD", "EUR", "CHF", "GBP" };

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.ActionArguments.ContainsKey("currency"))
            {
                string currency = filterContext.ActionArguments["currency"] as string;

                if (!supportedCurrencies.Any(a => a.Equals(currency.ToUpper())))
                {
                    filterContext.Result =
                        new BadRequestObjectResult(
                            "The 'NBPInfo' service only supports currencies: " +
                            "USD, EUR, CHF, GBP. The '" + currency + "' is not supported"
                            );
                }
            }
            if (filterContext.ActionArguments.ContainsKey("startDate") &&
                filterContext.ActionArguments.ContainsKey("startDate")
               )
            {
                var startDate = filterContext.ActionArguments["startDate"] as DateTime?;
                var endDate = filterContext.ActionArguments["endDate"] as DateTime?;
                var datesDifferense = ((TimeSpan)(endDate - startDate));

                if (datesDifferense.TotalDays > 93)
                {
                    filterContext.Result =
                        new BadRequestObjectResult(
                            "The time interval can not be longer than 93 days. " +
                            "Your time interval is equal " + datesDifferense.TotalDays + " days."
                            );
                }

                if (startDate < new DateTime(2002, 1, 2))
                {
                    filterContext.Result =
                        new BadRequestObjectResult(
                            "The 'startDate' parameter must be equal or higher than '2002-01-02'."
                            );
                }
                if (endDate > DateTime.Now)
                {
                    filterContext.Result =
                        new BadRequestObjectResult(
                            "The 'endDate' parameter can not be higher than the actual time."
                            );
                }
            }
        }
    }
}
