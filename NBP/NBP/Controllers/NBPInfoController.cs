﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using NBP.Common;
using NBP.Models;
using NBP.Validations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;

namespace NBP.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class NBPInfoController : ControllerBase
    {
        public readonly IOptions<NBPConfig> _configuration;

        public NBPInfoController(IOptions<NBPConfig> configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public IEnumerable<string> Start()
        {
            return new string[] { "Service is working" };
        }

        [HttpGet("{currency}/{startDate}/{endDate}")]
        [InputDataValidation]
        public object Get(string currency, DateTime startDate, DateTime endDate)
        {
            using (var client = new HttpClient())
            {
                SetHttpClientParams(client);

                var serverResponse = client.GetAsync(
                    _configuration.Value.TableType + "/" + currency + "/" +
                    startDate.ToString(_configuration.Value.DateTimeFormat) + "/" +
                    endDate.ToString(_configuration.Value.DateTimeFormat) + "/"
                    ).Result;

                if (serverResponse.IsSuccessStatusCode)
                {
                    var respons = Deserialize<NBPServiceRespons>(serverResponse.Content.ReadAsStringAsync().Result);
                    var averagePrice = Helper.CalculateAveragePrice(respons.rates);
                    var standardDeviation = Helper.CalculateStandardDeviation(respons.rates);
                    var nbpInfoRespons = new NBPInfoResponse
                    {
                        average_price = averagePrice,
                        standard_deviation = standardDeviation
                    };

                    return JsonConvert.SerializeObject(nbpInfoRespons, Formatting.Indented);
                }
                else
                {
                    return new BadRequestObjectResult(
                            "The NBP service respons: " + serverResponse.StatusCode
                            );
                }
            }
        }

        private void SetHttpClientParams(HttpClient client)
        {
            client.DefaultRequestHeaders.Clear();
            client.BaseAddress = new Uri(_configuration.Value.Baseurl);
        }

        private T Deserialize<T>(string json)
        {
            JsonSerializer s = new JsonSerializer();
            return s.Deserialize<T>(new JsonTextReader(new StringReader(json)));
        }
    }
}
