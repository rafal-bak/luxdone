﻿namespace NBP
{
    public class NBPConfig
    {
        public string DateTimeFormat { get; set; }
        public string Baseurl { get; set; }
        public string TableType { get; set; }
    }
}
