﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NBP.Models
{
    public class Rate
    {
        public string no { get; set; }
        public DateTime effectiveDate { get; set; }
        public double bid { get; set; }
        public double ask { get; set; }
    }
}
