﻿namespace NBP.Models
{
    public class NBPInfoResponse
    {
        public double standard_deviation { get; set; }
        public double average_price { get; set; }
    }
}
