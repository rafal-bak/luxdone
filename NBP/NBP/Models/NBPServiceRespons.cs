﻿using System.Collections.Generic;

namespace NBP.Models
{
    public class NBPServiceRespons
    {
        public string table { get; set; }
        public string currency { get; set; }
        public string code { get; set; }
        public List<Rate> rates { get; set; }
    }
}
