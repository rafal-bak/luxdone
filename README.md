# README #

To properly run the 'NBPINFO' service in the Developers environment, proceed with the instructions.

### Instruction ###

* Open in Visual Studion 'NBP.sln'
* Run service.


* Detailing web browsers add to the URL:
* '/{currency}/{startDate}/{endDate}' => /EUR/2013-01-28/2013-01-31/

* Detailing postman type URL like below:
* full URL example => https://localhost:5001/NBPInfo/EUR/2013-01-28/2013-01-31/ 

